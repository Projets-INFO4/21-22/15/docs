# Rust Parallel : Current status of the project

## Main implemented features of the project :

### Parser
The parser, an important element to build AST for parallel commands, is already implemented with the Pest framework. As the program is working, it seems like the parser is correct, and we can use it without changing it.

### Interpretation of commands
Command separed of values with ‘:::’, and double brackets are replaced by values as it is the case in GNU Parallel.  

*Example:*  
```parallel sleep {}';' echo {} done ::: 5 4 3 2 1```

### Option *--keep-order*
Each command are executed simultaneously, but they return their results in the same order they’ve been called.  
*Exemple:*  
```parallel sleep {}';' echo {} done ::: 5 4 3 2 1``` will return :
```
5 done
4 done
3 done
2 done
1 done
```
Obviously, the command with sleep 5 is the longest. But others don’t display before it as we gave the value 5 in first.

### Option *--dry-run*
Doesn’t execute the given command, but displays in detail all the sub-commands that will be executed.  
*Example:*  
```parallel –dry-run wc -l ::: example.*``` 
gives the output :  
```
wc -l ::: example.1
wc -l ::: example.2
wc -l ::: example.3
wc -l ::: example.4
wc -l ::: example.5
```
(supposing that we have all those 5 files in the current folder).

### Option *--jobs N*
Allow the program to be run in parallel on N cores of the CPU, the default mode of parallel uses the maximum cores available on the computer

### Execution on a remote machine
We know that the feature is available, while perfectible, but we currently have to read the corresponding part of the code in depth.

## What to do now ?

### Pipe flag
One of the main features of GNU Parallel is missing : the pipe flag.  
Similar to UNIX pipe, it may be used to use input from a previous command to use it with the command we want to execute with parallel.  
*Exemple:*  
```seq 1000000 | parallel --pipe wc```  
gives the output :  
```
165668 165668 1048571
149796 149796 1048572
149796 149796 1048572
149796 149796 1048572
149796 149796 1048572
149796 149796 1048572
85352 85352 597465
```

### Remote execution
At the moment, the flag --jobs cannot be used with the remote execution. According to the report of the previous group :  
>“Because we have to create the Tokio’s runtime at the Server creation, we cannot decide a maximal number of threads running for the execution of the request.”  

We also have to make a little optimization : the messages exchanged between a client and a server during a remote execution are strings. We could implement integer constants that have defined meanings rather than strings. It would reduce the quantity of data exchanged on the network.  

Finally, according to the previous group, we could also modify the Channel object in order to make it more efficient and usable more easily.  

Another idea :  
Error handling : timeout…
