## Authors

Florian CHAPPAZ\
Valentin DE OLIVEIRA\
Fikret KÜRKLÜ

# Rust Parallel : Project tracking

This project originally started on January 25, 2021.\
We took over on January 17, 2022.

## January 17, 2022

 - We started learning of Rust.
 -- [The Rust Programming Language](https://doc.rust-lang.org/book/)

## January 24, 2022

- Q/A session with Olivier RICHARD to confirm our understanding of the project. We planned to make a synthesis of [the existing project](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/20-21/14/rust-parallel) , its implementation and the features it contains compared to [GNU Parallel](https://www.gnu.org/software/parallel/).
- Rust Learning + Exercises with [rustlings](https://github.com/rust-lang/rustlings).

## January 31, 2022

- Pursuing Rust learning.

## February 7, 2022

- Ending [The Rust Programming Language](https://doc.rust-lang.org/book/) book.
- Quick overview on [Tokio](https://github.com/tokio-rs/tokio) library.
- GNU Parallel overview : Chapter 2 of [GNU Parallel 2018](https://zenodo.org/record/1146014#.YgEqji_pNQI).

## February 14, 2022

- Synthesis about the project status : what's already working, what we have to add or to modify...
- Presentation of the [synthesis](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/21-22/15/docs/-/blob/main/synthesis.md) with Olivier Richard.
- Preparation of the mid presentation

## February 28, 2022

- Preparation of mid-semester presentation : from the previous synthesis, i.e. what has been done, what we've done and what we're going to do.
- Presentation
- Rework of the presentation, focusing more of the global presentation of the project than on technical points, in order to be understood by everyone.

## March 1, 2022

- Code review in group on main parts of the code : interpreter, remote handling.

## March 7, 2022

- Understanding of the given code, making of a class diagram for having a better visualization of the project
- Beginning of the part that will handle pipe

## March 8, 2022

- Work and reflexion on the pipe option

## March 14, 2022

- Different ideas about Pipe option :
    -- Hidden file : No.
    -- Understanding the "block" concept, and tests about reading stdin in the program.
- Working on Tokio and the C/S part

## March 15, 2022

- Pipe option : Working correctly for input of size below the size of a block.
- Problems when dividing the input in several blocks, and bash argument size still too long (os error 7).

## March 21, 2022

- Pipe option : worked on dividing standard input in blocks.
- Working on replacing strings by constant integer in networks exchanges.

## March 22, 2022

- Pipe option : finishing implementation to print good values and good blocks : it raised an invisible problem. The keep order option, that were supposed to work, was not taken in account.
- Working on replacing strings by constant integer in networks exchanges.

# March 28, 2022

- Working on fixing the keep-order option. FIXED
- Working on pipe option : poor performances.
- Working on replacing strings by constant integer in networks exchanges.

